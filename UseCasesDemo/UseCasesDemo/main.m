//
//  main.m
//  UseCasesDemo
//
//  Created by System Admin on 27/04/2017.
//  Copyright © 2017 System Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
