//
//  UseCaseConstraints.m
//  1TO1
//
//  Created by System Admin on 21/04/2017.
//  Copyright © 2017 Wylog. All rights reserved.
//

#import "UseCaseConstraints.h"
#import "UIView+AutolayoutUtility.h"

@interface UseCaseConstraints()
{
    
    
    
}

@property(strong,nonatomic) UIView *selfView;

@property(strong,nonatomic) NSMutableArray *verticalArray;
@property(strong,nonatomic) NSMutableArray *horizontalArray;
@property(strong,nonatomic) NSMutableArray *constraintArray;
@property(strong,nonatomic) NSDictionary *callerParam;
@property(strong,nonatomic) NSDictionary *calledParam;
@property(strong,nonatomic) NSDictionary *callViewParam;
@property(strong,nonatomic) NSDictionary *coverViewParam;
@property(strong,nonatomic) NSDictionary *blurViewParam;
@end

@implementation UseCaseConstraints
CGFloat screenWidth;
CGFloat screenHeight;
BOOL isCallViewRaised = NO;
BOOL isPortraitBlur = NO;

- (void)setScreenWidth
{
    _constraintArray = [NSMutableArray new];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
}

- (void)useCoverViewParam:(UIView *)coverView
{
    self.coverViewParam = @{@"view":coverView};
}

- (void)useCalledViewParam:(UIView *)calledView
{
    self.calledParam = @{@"view":calledView};
}

- (void)useCallerViewParam:(UIView *)callerView
{
    self.callerParam = @{@"view":callerView};
}

- (void)useCallViewParam:(UIView *)callsView
{
    self.callViewParam = @{@"view":callsView};
}
- (void)useBlurViewParam:(UIView *)blurView
{
    self.blurViewParam = @{@"view":blurView};
}


- (void)setView:(UIView *)view
{
    _selfView = view;
}




/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */



-(void)handleCase:(kUseCases)useCase
{
    
    isCallViewRaised = NO;
    isPortraitBlur = YES;
    if(!_constraintArray)
        _constraintArray = [NSMutableArray new];
    else
        [_constraintArray removeAllObjects];
    switch (useCase) {
        case kFlatto360Landscape:
            isCallViewRaised = NO;
            isPortraitBlur = NO;
            [self setViewConstraint:kContactScreenFlatto360Landscape type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreviewFlatLandscape type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenUnraisedLandscape type:kCallViewConstraint];
            [self setViewConstraint:kCoverViewScreenPortrait type:kCoverViewConstraint];
            break;
        case k360to360Portrait:
            isCallViewRaised = YES;
            [self setViewConstraint:kContactScreen360to360 type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreview360BottomPortrait type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenRaised type:kCallViewConstraint];
            break;
        case k360to360Landscape:
            
            [self setViewConstraint:kContactScreen360 type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreview360Landscape type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenUnraisedLandscape type:kCallViewConstraint];
            break;
        case k360toFlatPortrait:
            isCallViewRaised = YES;
            [self setViewConstraint:kContactScreen360toFlatPortrait type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreview360BottomPortrait type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenRaised type:kCallViewConstraint];
            
            break;
        case kFlatto360Portrait:
            isCallViewRaised = YES;
            [self setViewConstraint:kContactScreenFlatto360Portrait type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreview360BottomPortrait type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenUnraisedPortrait type:kCallViewConstraint];
            [self setViewConstraint:kCoverViewScreenPortrait type:kCoverViewConstraint];
            break;
        case k360toFlatLandscape:
            isPortraitBlur = NO;
            [self setViewConstraint:kContactScreenFlattoFlat type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreviewFlatLandscape type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenUnraisedLandscape type:kCallViewConstraint];
            
            break;
        case kFlattoFlatPortrait:
            [self setViewConstraint:kContactScreenFlattoFlat type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreviewFlatPortrait type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenUnraisedPortrait type:kCallViewConstraint];
            break;
        case kFlattoFlatLandscape:
            [self setViewConstraint:kContactScreenFlattoFlat type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreviewFlatLandscape type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenUnraisedLandscape type:kCallViewConstraint];
            break;
        default:
            [self setViewConstraint:kContactScreenFlattoFlat type:kContactScreenConstraint];
            [self setViewConstraint:kUserPreviewFlatLandscape type:kUserPreviewConstraint];
            [self setViewConstraint:kCallViewScreenUnraisedLandscape type:kCallViewConstraint];
            break;
    }
    
    [self addAllConstraints];
}





-(void)setViewConstraint:(int)constraint type:(NSString *)type {
    
    
    
    NSArray *visualFormat;
    NSDictionary *viewParam;
    NSArray *horizontal;
    NSArray *vertical;
    [(UIView *)[self.coverViewParam objectForKey:@"view"] setHidden:YES];
    if(![type isEqualToString:kCallViewConstraint])
    {
        if([type isEqualToString:kContactScreenConstraint])
        {
            viewParam = self.callerParam;
            [(UIView *)[self.callerParam objectForKey:@"view"] removeAllConstraints];
            visualFormat = [[self contactScreenConstraints:constraint] objectForKey:kContactScreenConstraint];
        }
        else if([type isEqualToString:kUserPreviewConstraint])
        {
            [(UIView *)[self.calledParam objectForKey:@"view"] removeAllConstraints];
            viewParam = self.calledParam;
            visualFormat = [[self userPreviewConstraints:constraint] objectForKey:kUserPreviewConstraint];
        }
        else
        {
            [(UIView *)[self.coverViewParam objectForKey:@"view"] setHidden:NO];
            [(UIView *)[self.coverViewParam objectForKey:@"view"] removeAllConstraints];
            visualFormat = @[[NSString stringWithFormat:@"H:[view(375)]-150-|"],[NSString stringWithFormat:@"V:|-0-[view(370)]"]];
            viewParam = self.coverViewParam;
            horizontal =  [NSLayoutConstraint
                           constraintsWithVisualFormat:visualFormat[0]
                           options:NSLayoutFormatDirectionLeadingToTrailing
                           metrics:nil
                           views:viewParam];
            vertical = [NSLayoutConstraint
                        constraintsWithVisualFormat:visualFormat[1]
                        options:NSLayoutFormatDirectionLeadingToTrailing
                        metrics:nil
                        views:viewParam];
            [_constraintArray addObject:vertical];
            [_constraintArray addObject:horizontal];
            
            [(UIView *)[self.blurViewParam objectForKey:@"view"] setHidden:NO];
            [(UIView *)[self.blurViewParam objectForKey:@"view"] removeAllConstraints];
            
            
            viewParam = self.blurViewParam;
            
        }
        horizontal =  [NSLayoutConstraint
                                constraintsWithVisualFormat:visualFormat[0]
                                options:NSLayoutFormatDirectionLeadingToTrailing
                                metrics:nil
                                views:viewParam];
        vertical = [NSLayoutConstraint
                             constraintsWithVisualFormat:visualFormat[1]
                             options:NSLayoutFormatDirectionLeadingToTrailing
                             metrics:nil
                             views:viewParam];
    }
    else
    {
        [(UIView *)[self.callViewParam objectForKey:@"view"] removeAllConstraints];
        viewParam = self.callViewParam;
        int margin;
        if(constraint == kCallViewScreenUnraisedPortrait ||  kCallViewScreenRaised)
            margin = (screenWidth - 213)/2;
        if(constraint == kCallViewScreenUnraisedLandscape)
            margin = (screenHeight - 213)/2;
        NSString *marginStr = [NSString stringWithFormat:@"H:|-%d-[view(213)]-%d-|",margin,margin];
        
        
        
        if(isCallViewRaised)
        {
            visualFormat = @[marginStr,[NSString stringWithFormat:@"V:[view(70)]-75-|"]];
            
        }
        else
        {
            visualFormat = @[marginStr,[NSString stringWithFormat:@"V:[view(70)]-20-|"]];
        }
        horizontal =  [NSLayoutConstraint
                                constraintsWithVisualFormat:visualFormat[0]
                                options:NSLayoutFormatAlignAllCenterY
                                metrics:nil
                                views:viewParam];
        vertical = [NSLayoutConstraint
                             constraintsWithVisualFormat:visualFormat[1]
                             options:NSLayoutFormatDirectionLeadingToTrailing
                             metrics:nil
                             views:viewParam];
        
        
    }
    
    
    
    _verticalArray = [NSMutableArray arrayWithArray:vertical];
    _horizontalArray = [NSMutableArray arrayWithArray:horizontal];
    [_constraintArray addObject:_verticalArray];
    [_constraintArray addObject:_horizontalArray];
    
    
}



- (void)addAllConstraints
{
    for(int n = 0;n < [_constraintArray count];n++)
    {
        [_selfView addConstraints:_constraintArray[n]];
    }
    
}

- (NSArray *)getConstraintArray
{
    return _constraintArray;
}

- (NSArray *)getVertical
{
    return _verticalArray;
}
- (NSArray *)getHorizontal
{
    return _horizontalArray;
}

- (NSDictionary *)userPreviewConstraints:(kUserPreview)type
{
    NSArray *userPreviewConstraint;
    switch (type) {
        case kUserPreviewFlatPortrait:
            userPreviewConstraint = @[[NSString stringWithFormat:@"H:[view(90)]-30-|"],[NSString stringWithFormat:@"V:|-30-[view(160)]"]];
            break;
        case kUserPreviewFlatLandscape:
            userPreviewConstraint = @[[NSString stringWithFormat:@"H:[view(160)]-30-|"],[NSString stringWithFormat:@"V:|-30-[view(90)]"]];
            break;
        case kUserPreview360Landscape:
            userPreviewConstraint = @[[NSString stringWithFormat:@"H:[view(240)]-30-|"],[NSString stringWithFormat:@"V:|-30-[view(80)]"]];
            break;
        case kUserPreview360BottomPortrait:
            userPreviewConstraint = @[[NSString stringWithFormat:@"H:[view(375)]-0-|"],[NSString stringWithFormat:@"V:|-560-[view(125)]"]];
            break;
        default:
            userPreviewConstraint = @[[NSString stringWithFormat:@"H:[view(90)]-30-|"],[NSString stringWithFormat:@"V:|-30-[view(160)]"]];
            break;
    }
    
    NSDictionary *constraintsList = @{kUserPreviewConstraint:userPreviewConstraint};
    return constraintsList;
}

- (void)withoutUi
{
    [[_calledParam objectForKey:@"view"] setHidden:YES];
    [[_callViewParam objectForKey:@"view"] setHidden:YES];
}

- (void)withUi
{
    [[_calledParam objectForKey:@"view"] setHidden:NO];
    [[_callViewParam objectForKey:@"view"] setHidden:NO];
}

- (NSDictionary *)contactScreenConstraints:(kContactScreen)type
{
    NSArray *contactScreenConstraint;
    NSString *marginStrVertical,*marginStrHorizontal;
    int margin = (screenHeight - 370)/2;
    int marginvert = (screenHeight - 370 - 125)/2;
    marginStrHorizontal = [NSString stringWithFormat:@"H:|-%d-[view(375)]-%d-|",margin,margin];
    marginStrVertical = [NSString stringWithFormat:@"V:|-%d-[view(375)]",marginvert];
    switch (type) {
        case kContactScreen360:
            contactScreenConstraint = @[[NSString stringWithFormat:@"H:|-0-[view]-0-|"],[NSString stringWithFormat:@"V:|-0-[view]-0-|"]];
            break;
        case kContactScreenFlattoFlat:
            contactScreenConstraint = @[[NSString stringWithFormat:@"H:|-0-[view]-0-|"],[NSString stringWithFormat:@"V:|-0-[view]-0-|"]];
            break;
        case kContactScreenFlatto360Portrait:
            contactScreenConstraint = @[[NSString stringWithFormat:@"H:[view(375)]-0-|"],marginStrVertical];
            break;
        case kContactScreenFlatto360Landscape:
            
            contactScreenConstraint = @[marginStrHorizontal,[NSString stringWithFormat:@"V:|-0-[view]-0-|"]];
            break;
        case kContactScreen360to360:
            contactScreenConstraint = @[[NSString stringWithFormat:@"H:[view(375)]-0-|"],[NSString stringWithFormat:@"V:|-0-[view(562)]|"]];
            break;
        default:
            contactScreenConstraint = @[[NSString stringWithFormat:@"H:|-0-[view]-0-|"],[NSString stringWithFormat:@"V:|-0-[view]-0-|"]];
            break;
    }
    
    NSDictionary *constraintsList = @{kContactScreenConstraint:contactScreenConstraint};
    return constraintsList;
}



@end
