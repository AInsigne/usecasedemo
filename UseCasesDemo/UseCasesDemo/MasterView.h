//
//  MasterView.h
//  UseCasesDemo
//
//  Created by System Admin on 01/05/2017.
//  Copyright © 2017 System Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CallButtonsView.h"
@interface MasterView : UIView
- (UIImageView *)getCalledViewContainer;
- (UIImageView *)getCallerViewContainer;
- (CallButtonsView *)getCallButtonsView;
- (UIImageView *)getCoverViewContainer;
- (UIVisualEffectView *)getUIVisualEffectView;
@end
