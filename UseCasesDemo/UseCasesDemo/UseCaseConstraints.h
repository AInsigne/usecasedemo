//
//  UseCaseConstraints.h
//  1TO1
//
//  Created by System Admin on 21/04/2017.
//  Copyright © 2017 Wylog. All rights reserved.
//


typedef enum {
    kUserPreviewFlatPortrait,
    kUserPreview360BottomPortrait,
    kUserPreviewFlatLandscape,
    kUserPreview360Landscape,
} kUserPreview;

typedef enum {
    kContactScreenFlattoFlat,
    kContactScreenFlatto360Portrait,
    kContactScreen360toFlatPortrait,
    kContactScreenFlatto360Landscape,
    kContactScreen360,
    kContactScreen360to360,
    
} kContactScreen;

typedef enum {
    kCallViewScreenUnraisedPortrait,
    kCallViewScreenRaised,
    kCallViewScreenUnraisedLandscape,
} kCallViewScreen;

typedef enum {
    kCoverViewScreenPortrait,
    kCoverViewScreenLandscape,
    
    
    
} kCoverScreen;

typedef enum {
    k360toFlatPortrait,
    kFlattoFlatPortrait,
    k360to360Portrait,
    kFlatto360Portrait,
    k360toFlatLandscape,
    kFlattoFlatLandscape,
    k360to360Landscape,
    kFlatto360Landscape
} kUseCases;


typedef enum {
    kUser,
    kContact
} kUserTypes;




#import <UIKit/UIKit.h>
static NSString *const kUserPreviewConstraint = @"kUserPreview";
static NSString *const kContactScreenConstraint = @"kContactScreen";
static NSString *const kCoverViewConstraint = @"kCoverView";
static NSString *const kCallViewConstraint = @"kCallView";





@interface UseCaseConstraints : UIView


- (NSArray *)getConstraintArray;
- (void)handleCase:(kUseCases)useCase;
- (void)setScreenWidth;
- (void)setView:(UIView *)view;
- (NSArray *)getHorizontal;
- (NSArray *)getVertical;
- (void)withoutUi;
- (void)withUi;
- (void)useCalledViewParam:(UIView *)calledView;
- (void)useCallerViewParam:(UIView *)callerView;
- (void)useCallViewParam:(UIView *)callView;
- (void)useCoverViewParam:(UIView *)coverView;
- (void)useBlurViewParam:(UIView *)blurView;
@end
