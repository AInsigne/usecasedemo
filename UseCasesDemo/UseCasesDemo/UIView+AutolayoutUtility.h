//
//  UIView+AutolayoutUtility.h
//  POC2
//
//  Created by Naz Mariano on 09/11/2016.
//  Copyright © 2016 Wylog. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AutolayoutUtility)
- (void)removeAllConstraints;
@end
