//
//  UIView+AutolayoutUtility.m
//  POC2
//
//  Created by Naz Mariano on 09/11/2016.
//  Copyright © 2016 Wylog. All rights reserved.
//

#import "UIView+AutolayoutUtility.h"

@implementation UIView (AutolayoutUtility)
- (void)removeAllConstraints
{
    UIView *superview = self.superview;
    while (superview != nil) {
        for (NSLayoutConstraint *c in superview.constraints) {
            if (c.firstItem == self || c.secondItem == self) {
                [superview removeConstraint:c];
            }
        }
        superview = superview.superview;
    }
    
    [self removeConstraints:self.constraints];
}
@end
