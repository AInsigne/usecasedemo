//
//  ViewController.m
//  UseCasesDemo
//
//  Created by System Admin on 27/04/2017.
//  Copyright © 2017 System Admin. All rights reserved.
//

#import "ViewController.h"
#import "MasterView.h"
#import "UseCaseConstraints.h"
@interface ViewController ()
{
    UseCaseConstraints *useCases;
    
    __weak IBOutlet MasterView *masterView;
    
    __weak IBOutlet UIButton *withoutUiText;
    
    NSArray *arraySwitchText;
    __weak IBOutlet UIButton *switchToText;
}
@end

@implementation ViewController
int count = 0;
BOOL isNoUI = NO;
- (IBAction)switchUseCase:(id)sender {
    count++;
    [switchToText setTitle:arraySwitchText[count + 1] forState:UIControlStateNormal];
    [self useCaseFlat:self.view.frame.size];
}

- (IBAction)withoutUiToggle:(id)sender {
    isNoUI = !isNoUI;
    if(isNoUI)
        [useCases withoutUi];
    else
        [useCases withUi];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    arraySwitchText = @[@"Switch To Flat to Flat",@"Switch To Flat to 360",@"Switch To 360 to Flat",@"Switch To 360 to 360",@"Switch To Error 1",@"Switch To Error 2",@"Switch To Error 3"];
    useCases = [UseCaseConstraints new];
    [useCases setView:masterView];
    [useCases setScreenWidth];
    [useCases useCallViewParam:[masterView getCallButtonsView]];
    [useCases useCalledViewParam:[masterView getCalledViewContainer]];
    [useCases useCallerViewParam:[masterView getCallerViewContainer]];
    [useCases useCoverViewParam:[masterView getCoverViewContainer]];
    [useCases useBlurViewParam:[masterView getUIVisualEffectView]];
    
    [useCases handleCase:kFlattoFlatPortrait];
    [switchToText setTitle:arraySwitchText[count + 1] forState:UIControlStateNormal];
    //[self.view bringSubviewToFront:withoutUiText];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         
         // do whatever
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         if(count == 0)
             [self useCaseFlat:size];
         if(count == 1)
             [self useCaseFlat360:size];
         if(count == 2)
             [self useCase360Flat:size];
         if(count == 3)
             [self useCase360360:size];
         
     }];
}

- (void)useCaseFlat:(CGSize)size
{
    if(size.width>size.height)
    {
        [useCases handleCase:kFlattoFlatLandscape];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat horizontal"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat landscape"]];
    }
    else
    {
        [useCases handleCase:kFlattoFlatPortrait];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat"]];
    }
}

- (void)useCaseFlat360:(CGSize)size
{
    if(size.width>size.height)
    {
        [useCases handleCase:kFlatto360Landscape];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat horizontal"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat landscape"]];
    }
    else
    {
        [useCases handleCase:kFlatto360Portrait];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat landscape"]];
    }
}

- (void)useCase360Flat:(CGSize)size
{
    if(size.width>size.height)
    {
        [useCases handleCase:k360toFlatLandscape];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat horizontal"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat landscape"]];
    }
    else
    {
        [useCases handleCase:k360toFlatPortrait];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat landscape"]];
    }
}
- (void)useCase360360:(CGSize)size
{
    if(size.width>size.height)
    {
        [useCases handleCase:k360to360Landscape];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat horizontal"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat landscape"]];
    }
    else
    {
        [useCases handleCase:k360to360Portrait];
        [masterView.getCallerViewContainer setImage:[UIImage imageNamed:@"vincent flat"]];
        [masterView.getCalledViewContainer setImage:[UIImage imageNamed:@"leandre flat landscape"]];
    }
}

@end
