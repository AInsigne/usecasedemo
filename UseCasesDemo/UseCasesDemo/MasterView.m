//
//  MasterView.m
//  UseCasesDemo
//
//  Created by System Admin on 01/05/2017.
//  Copyright © 2017 System Admin. All rights reserved.
//

#import "MasterView.h"

@interface MasterView(){
    
    __weak IBOutlet UIImageView *calledViewContainer;
    
    __weak IBOutlet UIVisualEffectView *blurView;
    
    __weak IBOutlet UIImageView *callerViewContainer;
    
    __weak IBOutlet CallButtonsView *callButtonsView;
    
    __weak IBOutlet UIImageView *coverViewContainer;
    
    
    
}

@end

@implementation MasterView

- (UIImageView *)getCalledViewContainer
{
    return calledViewContainer;
}
- (UIImageView *)getCallerViewContainer
{
    return callerViewContainer;
}
- (UIImageView *)getCoverViewContainer
{
    return coverViewContainer;
}
- (CallButtonsView *)getCallButtonsView
{
    return callButtonsView;
}
- (UIVisualEffectView *)getUIVisualEffectView
{
    return blurView;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self addSubview:[[[NSBundle mainBundle] loadNibNamed:@"MasterView" owner:self options:nil] objectAtIndex:0]];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    NSLog(@"initWithFrame was called");  // this was called
    if (self) {
        // Initialization code
        [self addSubview:[[[NSBundle mainBundle] loadNibNamed:@"MasterView" owner:self options:nil] objectAtIndex:0]];
    }
    return self;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
